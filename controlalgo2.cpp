#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Structure du jeu qui qontient les informations du jeu.
struct Jeu {
    int combiAleatoire[4];
    int combiJoueur[4];
    int nombreEssai=10;
    int nombrePartieGagnee=0;
    int nombrePartiePerdue=0;
};


int main()
{
    srand(time(NULL));
    Jeu mastermind;
    bool jouerencore = true;

    while (jouerencore) {
        mastermind.nombreEssai=10;
        //fonction qui g�n�re une combinaison al�atoire
        for(int i=0; i<4; i++)
        {
            mastermind.combiAleatoire[i] = rand()%4;
        }

        //Boucle qui permet de jouer tant que le nombre d'essai 10 n'est pas atteind
        while(mastermind.nombreEssai>0)
        {

            cout << "Il reste " << mastermind.nombreEssai << " essai(s)" << endl;

            for(int i=0; i<4; i++)
            {
                cout << "Couleur " << i+1 << ": ";
                cin >> mastermind.combiJoueur[i];
                cout << endl;
            }

            //Ici on v�rifie les choix du joueur avec la combinaison secr�te
            int nbBienPlace = 0;
            int nbMalPlace = 0;

            for(int i=0; i<4; i++)
            {
                if(mastermind.combiAleatoire[i] == mastermind.combiJoueur[i]) {
                    nbBienPlace++;
                    cout<<"Score des biens place = " << nbBienPlace << endl;
                } else {
                    //Tester si mal plac�
                    nbBienPlace--;
                    cout<<"Score des mal place = " << nbMalPlace << endl;
                }
            }

            //Ici on test la combinaison du joueur et si elle est juste on le f�licite
            if(nbBienPlace==4)
            {
                cout << "Vous avez gagne ! Nombre d'essai restants: " << mastermind.nombreEssai << endl;
                mastermind.nombrePartieGagnee++;
                break;
            }
            else
            {
                cout << "Non ce n'est pas  cela." << endl << endl;
            }

            mastermind.nombreEssai--;
        }



        if (mastermind.nombreEssai == 0)
        {
            cout << "Vous avez perdu !" << endl;
            mastermind.nombrePartiePerdue++;
        }

        //Afficher nombre parties gagn�es et perdues
        cout << "Vous avez gagne : " << mastermind.nombrePartieGagnee << " partie(s) !" << endl;
        cout << "Vous avez perdu : " << mastermind.nombrePartiePerdue << " partie(s) !" << endl << endl;

        //Demander si rejouer
        char entree;
        bool resultat=0;
        //Boucle qui permet de rejouer en fin de partie
        do
        {
            cout << "\nVoulez vous rejouer ? (o - oui, n - non); ";
            cin >> entree;
            if (entree == 'O' || entree == 'o' || entree == 'n' || entree == 'N')
            {
                resultat = 1;
                if (entree == 'O' || entree == 'o')
                {
                    jouerencore = true;
                } else {
                    jouerencore = false;
                }
            }
            else
            {
                cout <<"\nVous n'avez pas entrez un choix correct, s'il vous plait recommencer." << endl << endl;
                cout << "Fin de la partie ";
            }
        }
        while (!resultat);
    }


    return 0;
}
